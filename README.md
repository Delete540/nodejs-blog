nodejs-blog
===========

nodejs,express4.x+MongoDB+jade+Bootstrap

用express4.2.0做的小博客,目前是刚刚完成核心部分,还没有进行功能扩展,方案与设想已经有了并正在尝试弄出来;
数据库用的是MongoDB,才刚刚学着用,还有很多功能和语法不是很明白
总之跪求指导!

安装和运行
===========

先下载代码,安装MongoDB,本博客的数据库是"Blog",详见db.j和settings.js

安装依赖包(详见package.json)npm install

运行 node bin\www
