$("#back-to-top").hide();
//当滚动条的位置处于距顶部100像素以下时，跳转链接出现，否则消失
$(function () {
    $(window).scroll(function () {
        if ($(window).scrollTop() > 100) {
            $("#back-to-top").fadeIn(500);
            $(".hide-bar").fadeIn(500);
            $(".navbar").css({
                position:"fixed",
                zIndex:200
            });
            $(".userbtn,.loginbtn,.logout").css({
                position:"fixed",
                marginTop:-23,
                zIndex:200
            });
            $(".menubtn").css("marginTop",-10);
            if(isMobile.any()){
                $('body,html').unbind();
            }
            else{
                $(".main").stop().animate({marginTop:0},500);

            }

        }
        else {
            $("#back-to-top").fadeOut(500);
            $(".hide-bar").fadeOut(500);
            $(".navbar").css({
                position:"relative",
                zIndex:1
            });
            $(".userbtn,.loginbtn,.logout").css({
                position:"absolute",
                marginTop:-83,
                zIndex:198
            });
            $(".menubtn").css("marginTop",-3);
            if(isMobile.any()){
                $('body,html').unbind();
            }
            else{
                $(".main").stop().animate({marginTop:-50},500);

            }


        }

    });

    //当点击跳转链接后，回到页面顶部位置
    $("#back-to-top").click(function () {
        if(isMobile.any()){
            $('body,html').animate({ scrollTop: 0 }, 100);
        }
        else{
            $('body,html').animate({ scrollTop: 0 }, 1000);
        }


        return false;
    });
});

$(function(){
    var menuwidth=240,
        menuspeed=400,

        $body=$("body"),
        $lefthidemenu=$(".lefthidemenu"),
        $negwidth="-"+menuwidth+"px",
        $main=$(".main"),
        $poswidth=menuwidth+"px"

    //alert($(window).width());
        $(".menubtn").on("click",function(){

            if($lefthidemenu.css("display")=="none"){
                $lefthidemenu.animate({width: $poswidth}, menuspeed);
                $lefthidemenu.show();
                $(".overlay").show();
                $(".overlay").css({
                    top:52,
                    zIndex:198
                });

            }
            else{
                $lefthidemenu.animate({width: "0"}, menuspeed);
                $lefthidemenu.fadeOut();
                $(".overlay").hide();
                $(".overlay").css({
                    top:0,
                    zIndex:203
                });
            }

        })

        $(".overlay").on("click",function(){
            if($lefthidemenu.css("display")!="none"){
                $lefthidemenu.animate({width: "0"}, menuspeed);
                $lefthidemenu.fadeOut();
                $(".overlay").hide();
                $(".overlay").css({
                    top:0,
                    zIndex:203
                });
            }

        })






});
$(function(){




    var num=$(".box ul li").length;
    var startX = 0, startY = 0; //触摸开始时手势横纵坐标
    var temPos; //滚动元素当前位置
    var iCurr = 0; //当前滚动屏幕数
    var timer = null;
    var moveWidth = $(".box").width();
    var speed=30;
    var oPosition = {};
    //alert(num);
    $(".box").css({
        height:($(".box").width())/2,
        overflow: "hidden",
        marginTop:0,
        marginBottom:15,
        marginLeft:"auto",
        marginRight:"auto"
    }); //设定容器宽高及样式
    $(".box ul").css({
        position:"absolute",
        width:($(".box ul li img").width())*num*1.5

    })
    //alert()

    $(".box ul li img").css({
        width:$(".box").width(),
        height:"100%"
    })

    $(".box ul li").css({
        float:"left",
        display: 'inline'
    });
    $(".box").append('<div class="focus"><div></div></div>');
    $(".focus").css({
        width:($(".box ul").width()),
        height:parseInt(($(".box").height())/6),
        position:"absolute",
        bottom:0

    })
    var oFocusContainer = $(".focus");
    for (var i = 0; i < num; i++) {
        $("div", oFocusContainer).append("<span></span>");
    }
    var oFocus = $("span", oFocusContainer);
    $("span", oFocusContainer).css({
        display: 'block',
        float: 'left',
        cursor: 'pointer',
        width:parseInt($(".focus").height()/5),
        height:parseInt($(".focus").height()/5),
        marginRight:"10px",
        fontSize:0
    });

    $("div", oFocusContainer).width(parseInt(oFocus.outerWidth(true) * num+6)).css({
        position: 'absolute',
        right: parseInt(($(".focus").width())-($("div", oFocusContainer).width())-10)/2,
        top: '50%',
        marginTop: -(oFocus.height() / 2)
    });
    oFocus.first().addClass("current");
    //页面加载或发生改变
    $(window).bind('resize load', function(){
        mobileSettings();

        if (isMobile()) {
            bindTochuEvent();
        }

    });
    function bindTochuEvent(){
        $(".box ul").get(0).addEventListener('touchstart', touchStartFunc, false);
        $(".box ul").get(0).addEventListener('touchmove', touchMoveFunc, false);
        $(".box ul").get(0).addEventListener('touchend', touchEndFunc, false);
    }
    //获取触点位置
    function touchPos(e){
        var touches = e.changedTouches, l = touches.length, touch, tagX, tagY;
        for (var i = 0; i < l; i++) {
            touch = touches[i];
            tagX = touch.clientX;
            tagY = touch.clientY;
        }
        oPosition.x = tagX;
        oPosition.y = tagY;
        return oPosition;
    }
    //触摸开始
    function touchStartFunc(e){
        clearInterval(timer);
        touchPos(e);
        startX = oPosition.x;
        startY = oPosition.y;
        temPos = oMover.position().left;
    }
    //触摸移动
    function touchMoveFunc(e){
        touchPos(e);
        var moveX = oPosition.x - startX;
        var moveY = oPosition.y - startY;
        if (Math.abs(moveY) < Math.abs(moveX)) {
            e.preventDefault();
            $(".box ul").css({
                left: temPos + moveX
            });
        }
    }
    //触摸结束
    function touchEndFunc(e){
        touchPos(e);
        var moveX = oPosition.x - startX;
        var moveY = oPosition.y - startY;
        if (Math.abs(moveY) < Math.abs(moveX)) {
            if (moveX > 0) {
                iCurr--;
                if (iCurr >= 0) {
                    var moveX = iCurr * moveWidth;
                    doAnimate(-moveX, autoMove);
                }
                else {
                    doAnimate(0, autoMove);
                    iCurr = 0;
                }
            }
            else {
                iCurr++;
                if (iCurr < num && iCurr >= 0) {
                    var moveX = iCurr * moveWidth;
                    doAnimate(-moveX, autoMove);
                }
                else {
                    iCurr = num - 1;
                    doAnimate(-(num - 1) * moveWidth, autoMove);
                }
            }
            oFocus.eq(iCurr).addClass("current").siblings().removeClass("current");
        }
    }
    function mobileSettings(){
        moveWidth = $(".box").width();
        $(".box").css({
            position: "relative",
            height:($(".box").width())/2
        }); //设定容器宽高及样式

        $(".box ul").css({
            width:($(".box ul li img").width())*num*1.5
        });
        $(".box ul li img").css({
            width:$(".box").width()
        })
        $(".focus").css({
            width:($(".box ul li img").width()),
            height:parseInt(($(".box").height())/6)
        })
        $("div", oFocusContainer).width(parseInt(oFocus.outerWidth(true) * num+6)).css({
            position: 'absolute',
            right: parseInt(($(".focus").width())-($("div", oFocusContainer).width())-10)/2
        });
        $("span", oFocusContainer).css({
            width:parseInt($(".focus").height()/5),
            height:parseInt($(".focus").height()/5),
            marginRight:"10px"
        });
    }
    autoMove();
    //alert($(".box ul").get(0).offsetLeft);
    //PC机下焦点切换
    if (!isMobile()) {
        oFocus.hover(function(){
            iCurr = $(this).index() - 1;
            stopMove();
            doMove();
        }, function(){
            autoMove();
        })
    }
    function stopMove(){
        clearInterval(timer);
    }
    function autoMove(){
        timer = setInterval(doMove, 6000);
    }
    function doMove(){
        iCurr = iCurr >= num - 1 ? 0 : iCurr + 1;
        doAnimate(-moveWidth * iCurr);
        oFocus.eq(iCurr).addClass("current").siblings().removeClass("current");
    }
    //动画效果
    function doAnimate(iTarget, fn){
        $(".box ul").stop().animate({
            left: iTarget
        }, 1000, function(){
            if (fn)
                fn();
        });
    }


    function move(n){
        $('.box').css('left',  lastX - sw);
        index++;
    }
    //判断是否是移动设备
    function isMobile(){
        if (navigator.userAgent.match(/Android/i) || navigator.userAgent.indexOf('iPhone') != -1 || navigator.userAgent.indexOf('iPod') != -1 || navigator.userAgent.indexOf('iPad') != -1) {
            return true;
        }
        else {
            return false;
        }
    }
})



$(function(){
    $("body")
    $(".tips-btn").hover(
        function(){
            $(this).append("<div class='blogtips hidden-xs'><span class='glyphicon glyphicon-arrow-up' style='color:#1a1a1a; font-size: 17px; margin-top: -8px;margin-left:6px;  position: absolute;z-index:-1;')></span><span class='tips-words' style='line-height:25px ;'></span></div>")
            //$(".blogtips").show();
            $(".lo-tip").find(".blogtips").find(".tips-words").append("登陆");
            $(".lg-tip").find(".blogtips").find(".tips-words").append("注销");

        },
        function(){
           $(this).find(".blogtips").remove();
        }
    )
    var weekday=new Array(7)
    weekday[0]="星期日"
    weekday[1]="星期一"
    weekday[2]="星期二"
    weekday[3]="星期三"
    weekday[4]="星期四"
    weekday[5]="星期五"
    weekday[6]="星期六"
    var newdate=new Date(),
        day=weekday[newdate.getDay()],
        month=newdate.getMonth()+1,
        year=newdate.getFullYear(),
        hours=newdate.getHours(),
        minutes=newdate.getMinutes(),
        seconds=newdate.getSeconds(),
        tdate=newdate.getDate()

    $(".myblog-notice").eq(0).append("<div class='ndate'><span></span><small>日</small><span></span><span></span></div>").css({height:150});
    $(".myblog-notice span,.myblog-notice small").css("position","absolute");
    $(".myblog-notice span").eq(0).html(tdate).css({fontSize:70,marginTop:18,marginLeft:12});$(".notice-top small").eq(0).css({color:"#fff",marginTop:75,marginLeft:58,fontSize:16});
    $(".myblog-notice span").eq(1).html(year+"年"+month+"月").css({marginTop:5,marginLeft:0});
    $(".myblog-notice span").eq(2).html(day).css({marginTop:100,marginLeft:6,fontSize:20});

})

function elementHover(elm,elmclass){
    elm.hover(
        function(){
            $(this).addClass(elmclass);

        },
        function(){
            $(this).removeClass(elmclass);
        });
}

